(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.WebPayPlusParent = {
    attach: function (context, settings) {
      var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    	var eventer = window[eventMethod];
    	var messageEvent = eventMethod === "attachEvent"
    		? "onmessage"
    		: "message";
    	eventer(messageEvent, function (e) {
        if (e.data && e.data.toString().substring(0,16) == "paymentProcessed") {
          e.preventDefault();
          var separed = e.data.split('|');
          window.location.href = '/checkout/' + separed[2] + '/' + separed[3];
        }
    	});
    }
  };
}(jQuery, Drupal, drupalSettings));

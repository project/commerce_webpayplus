(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.WebPayPlusIframe = {
    attach: function (context, settings) {
      var returnpage = drupalSettings.commerce_webpayplus.returnpage || [];
      parent.postMessage("paymentProcessed|" + returnpage.status + '|' + returnpage.orderid + '|' + returnpage.step, "*");
    }
  };
}(jQuery, Drupal, drupalSettings));

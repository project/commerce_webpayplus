<?php

namespace Drupal\commerce_webpayplus\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_webpayplus\AesCrypto;
use Drupal\commerce_payment\CreditCard;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "webpayplus",
 *   label = @Translation("WebPayPlus"),
 *   display_label = @Translation("WebPayPlus"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_webpayplus\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   modes= {
 *     "staging" = "Staging",
 *     "live" = "Live"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   }
 * )
 */
class Webpayplus extends OffsitePaymentGatewayBase implements WebpayplusInterface {

  protected $verifyCount = 0;

  const WEBPAYPLUS_STAGING_URL = 'https://wppsandbox.mit.com.mx';

  /**
   * {@inheritdoc}
   */
  public function getCompany() {
    return $this->configuration['company'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBranch() {
    return $this->configuration['branch'];
  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->configuration['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPwd() {
    return $this->configuration['pwd'];
  }

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return $this->configuration['key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchant() {
    return $this->configuration['merchant'];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl() {
    return $this->configuration['baseurl'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'company' => 'SNBX',
      'branch' => '01SNBXBRNCH',
      'user' => 'SNBXUSR01',
      'pwd' => 'SECRETO',
      'key' => '5DCC67393750523CD165F17E1EFADD21',
      'merchant' => 'SNDBX123',
      'baseurl' => self::WEBPAYPLUS_STAGING_URL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#description' => $this->t('Enter your Company (id_company).'),
      '#default_value' => $this->getCompany(),
      '#required' => TRUE,
    ];

    $form['branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Branch'),
      '#description' => $this->t('Enter your Branch (id_branch).'),
      '#default_value' => $this->getBranch(),
      '#required' => TRUE,
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#description' => $this->t('Enter your User (user).'),
      '#default_value' => $this->getUser(),
      '#required' => TRUE,
    ];

    $form['pwd'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('Enter your Secret (password).'),
      '#default_value' => $this->getPwd(),
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Encrypt key'),
      '#description' => $this->t('Enter the encryption key (Semilla AES).'),
      '#default_value' => $this->getKey(),
      '#required' => TRUE,
    ];

    $form['merchant'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant'),
      '#description' => $this->t('Fixed string assigned to merchant (Data0).'),
      '#default_value' => $this->getMerchant(),
      '#required' => TRUE,
    ];

    $form['baseurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#description' => $this->t('The url to use (url).'),
      '#default_value' => $this->getBaseUrl(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['company'] = $values['company'];
      $this->configuration['branch'] = $values['branch'];
      $this->configuration['user'] = $values['user'];
      $this->configuration['pwd'] = $values['pwd'];
      $this->configuration['key'] = $values['key'];
      $this->configuration['merchant'] = $values['merchant'];
      $this->configuration['baseurl'] = $values['baseurl'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $return = parent::onReturn($order, $request);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $return = parent::onCancel($order, $request);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $str = $request->request->get('strResponse');
    if (!empty($str)) {
      $key = $this->getKey();
      $responseDecrypted = AESCrypto::desencriptar($str, $key);
      $xml = simplexml_load_string($responseDecrypted);
      $json = json_encode($xml);
      $result = json_decode($json, TRUE);
      if (!empty($result['response']) && $result['response'] == 'approved') {
        $order = $this->entityTypeManager->getStorage('commerce_order')->load((int) $result['reference']);
        if ($order) {
          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
          $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
          $payment = $payment_storage->loadByProperties(['remote_id' => $result['auth']]);
          if (!$payment) {
            $card_type = CreditCard::detectType(substr($result['cc_mask'], 0, 4));
            $payment_method = $payment_method_storage->create([
              'uid' => $order->getCustomerId(),
              'type' => 'credit_card',
              'payment_gateway' => $this->getPluginId(),
              'card_type' => $card_type,
              'card_number' => substr($result['cc_mask'], -4),
              'reusable' => FALSE,
              'expires' => '',
            ]);
            $payment_method->setBillingProfile($order->getBillingProfile());
            $payment_method->save();
            $order->payment_method = $payment_method;
            $order->save();
            $payment = $payment_storage->create([
              'state' => 'complete',
              'amount' => $order->getTotalPrice(),
              'payment_gateway' => $this->entityId,
              'order_id' => $order->id(),
              'remote_id' => $result['auth'],
              'remote_state' => $result['response'],
              'payment_method' => $payment_method,
            ]);
          }
          else {
            $payment->state = 'complete';
          }
          $payment->save();
          \Drupal::logger('commerce_webpayplus')->info('Payment information saved successfully. Transaction reference: ' . $result['foliocpagos']);
        }
      }

    }
  }

}

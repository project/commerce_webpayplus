<?php

namespace Drupal\commerce_webpayplus\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * Provides the interface for the Rave payment gateway.
 */
interface WebpayplusInterface extends OffsitePaymentGatewayInterface {

  /**
   * Get the configured Rave API Secret key.
   *
   * @return string
   *   The Rave API Secret key.
   */
  public function getCompany();

  /**
   * Get the configured Branch.
   *
   * @return string
   *   The Branch.
   */
  public function getBranch();

  /**
   * Get the configured User.
   *
   * @return string
   *   The User.
   */
  public function getUser();

  /**
   * Get the configured API Secret key.
   *
   * @return string
   *   The Secret key.
   */
  public function getPwd();

  /**
   * Get the configured API Encrypt key.
   *
   * @return string
   *   The Encrypt key.
   */
  public function getKey();

  /**
   * Get the configured API Merchant key.
   *
   * @return string
   *   The Merchant key.
   */
  public function getMerchant();

  /**
   * Get the base url to use for API requests based on the mode.
   *
   * @return string
   *   The Base URL.
   */
  public function getBaseUrl();

}

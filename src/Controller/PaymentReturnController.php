<?php

namespace Drupal\commerce_webpayplus\Controller;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Url;

/**
 * Provides the endpoint for payment notifications.
 */
class PaymentReturnController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The loogger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new EntityDuplicateController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The loogger factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('commerce_webpayplus');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Provides the "notify" page.
   *
   * Also called the "IPN", "status", "webhook" page by payment providers.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   The payment gateway.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function returnPage(PaymentGatewayInterface $commerce_payment_gateway, Request $request) {
    // http://lxc.libreriacarlosfuentes/payment/return/webpayplus?referencia=14&&nbResponse=Aprobado&nuAut=1.
    $this->logger->notice($this->t('Fetched request returnPage %params', ['%params' => print_r($request->query->all(), TRUE)]));
    $url = Url::fromRoute('commerce_webpayplus.return_redirection', ['commerce_payment_gateway' => $commerce_payment_gateway->id(), 'commerce_order' => $request->query->get('referencia')], ['query' => $request->query->all()]);
    return new RedirectResponse($url->toString());
  }

  /**
   * Redirection after notify page.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   The payment gateway.
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The payment gateway.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function returnRedirection(PaymentGatewayInterface $commerce_payment_gateway, OrderInterface $commerce_order, Request $request) {
    // http://lxc.libreriacarlosfuentes/payment/return/webpayplus?referencia=14&&nbResponse=Aprobado&nuAut=1&operacion=1.
    $this->logger->notice($this->t('Fetched request returnPage %params', ['%params' => print_r($request->query->all(), TRUE)]));
    $response['redirecting'] = [
      '#markup' => $this->t('Redirecting...'),
    ];
    $approved = $request->query->get('nbResponse') == 'Aprobado' && !empty($request->query->get('nuAut'));
    $computed_settings = [
      'orderid' => $request->query->get('referencia'),
      'nbResponse' => ($approved) ? 1 : 0,
    ];
    if (!$commerce_order) {
      throw new AccessDeniedHttpException();
    }
    else {
      $checkout_flow = $commerce_order->get('checkout_flow')->entity;
      $checkout_flow_plugin = $checkout_flow->getPlugin();
      $step_id = 'payment';
      if ($approved) {
        $payment_gateway = $commerce_order->get('payment_gateway')->entity;
        $payment_gateway_plugin = $payment_gateway->getPlugin();
        // Simulates the finalization of the payment.
        try {
          $payment_gateway_plugin->onReturn($commerce_order, $request);
          $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);
          $this->logger->notice($this->t('OK1 Redirect step %step', ['%step' => print_r($redirect_step_id, TRUE)]));
        }
        catch (PaymentGatewayException $e) {
          $this->logger->error($e->getMessage());
          $this->messenger->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
          $redirect_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
          $this->logger->notice($this->t('KO1 Redirect step %step', ['%step' => print_r($redirect_step_id, TRUE)]));
        }
        $commerce_order->set('checkout_step', $redirect_step_id);
        $this->onStepChange($redirect_step_id, $commerce_order);
        $commerce_order->save();
        $computed_settings['step'] = $redirect_step_id;
      }
      else {
        $payment_gateway = $commerce_order->get('payment_gateway')->entity;
        $payment_gateway_plugin = $payment_gateway->getPlugin();
        $payment_gateway_plugin->onCancel($commerce_order, $request);
        $redirect_step_id = $checkout_flow_plugin->getPreviousStepId($step_id);
        $commerce_order->set('checkout_step', $redirect_step_id);
        $this->onStepChange($redirect_step_id, $commerce_order);
        $commerce_order->save();
        $this->logger->notice($this->t('KO2 Redirect step %step', ['%step' => print_r($redirect_step_id, TRUE)]));
        $computed_settings['step'] = $redirect_step_id;
      }
    }
    $response['#attached']['library'][] = 'commerce_webpayplus/webpayplus_iframe';
    $response['#attached']['drupalSettings']['commerce_webpayplus']['returnpage'] = $computed_settings;
    return $response;
  }

  /**
   * Reacts to the current step changing.
   *
   * Called before saving the order and redirecting.
   *
   * Handles the following logic
   * 1) Locks the order before the payment page,
   * 2) Unlocks the order when leaving the payment page
   * 3) Places the order before the complete page.
   *
   * @param string $step_id
   *   The new step ID.
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The payment gateway.
   */
  protected function onStepChange($step_id, OrderInterface $commerce_order) {
    // Lock the order while on the 'payment' checkout step. Unlock elsewhere.
    if ($step_id == 'payment') {
      $commerce_order->lock();
    }
    elseif ($step_id != 'payment') {
      $commerce_order->unlock();
    }
    // Place the order.
    if ($step_id == 'complete' && $commerce_order->getState()->getId() == 'draft') {
      $commerce_order->getState()->applyTransitionById('place');
    }
  }

}

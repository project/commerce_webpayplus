<?php

namespace Drupal\commerce_webpayplus;

/**
 * AESCrypto utilities.
 */
class AesCrypto {

  /*
   * Mercadotecnia, Ideas y Tecnologia.
   * version 1.0
   * date 2017/10/10
   * En php.ini habilitar la linea extension=php_openssl.dll
   * (o equivalente a linux).
   */

  /**
   * Allows encrypt an string througth a key.
   *
   * @param string $plaintext
   *   The text to encrypt.
   * @param string $key128
   *   The key to encrypt.
   *
   * @return string
   *   Return the encrypted string.
   */
  public static function encriptar($plaintext, $key128) {
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
    $cipherText = openssl_encrypt($plaintext, 'AES-128-CBC', hex2bin($key128), 1, $iv);
    return base64_encode($iv . $cipherText);
  }

  /**
   * Allows decrypt an string from a key.
   *
   * @param string $encodedInitialData
   *   The text to decrypt.
   * @param string $key128
   *   The key to decrypt.
   *
   * @return string
   *   The decrypted string.
   */
  public static function desencriptar($encodedInitialData, $key128) {
    $encodedInitialData = base64_decode($encodedInitialData);
    $iv = substr($encodedInitialData, 0, 16);
    $encodedInitialData = substr($encodedInitialData, 16);
    $decrypted = openssl_decrypt($encodedInitialData, 'AES-128-CBC', hex2bin($key128), 1, $iv);
    return $decrypted;
  }

}

<?php

namespace Drupal\commerce_webpayplus\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_webpayplus\AesCrypto;

/**
 * {@inheritdoc}
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  protected $integrityHash;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $link = $this->decryptLink();
    if ($link) {
      $data = [
        'link' => $link,
      ];
      $form = $this->buildRedirectForm($form, $form_state, '', $data, '');
    }
    else {
      $form['error_message'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['error-message'],
        ],
        'message' => [
          '#markup' => $this->t('The link to WebPayPlus cant be generated.'),
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRedirectForm(array $form, FormStateInterface $form_state, $redirect_url, array $data, $redirect_method = BasePaymentOffsiteForm::REDIRECT_GET) {
    $form['iframe'] = [
      '#type' => 'inline_template',
      '#template' => '<iframe src="{{ url }}"></iframe>',
      '#context' => [
        'url' => $data['link'],
      ],
    ];
    $form['#attached']['library'][] = 'commerce_webpayplus/webpayplus_parent';
    return $form;
  }

  /**
   * Create the Chain text.
   */
  protected function generateChain() {
    $payment = $this->entity;
    $order = $payment->getOrder();
    $payment_amount = number_format($payment->getAmount()->getNumber(), 2, '.', '');
    $payment_currency = $payment->getAmount()->getCurrencyCode();
    $order_number = $order->id();
    $email = $order->getEmail();
    $plugin = $this->plugin;
    $company = $plugin->getCompany();
    $branch = $plugin->getBranch();
    $user = $plugin->getUser();
    $pwd = $plugin->getPwd();

    $chain = <<<CHAIN
<?xml version="1.0" encoding="UTF-8"?>
<P>
  <business>
    <id_company>$company</id_company>
    <id_branch>$branch</id_branch>
    <user>$user</user>
    <pwd>$pwd</pwd>
  </business>
  <url>
    <reference>$order_number</reference>
    <amount>$payment_amount</amount>
    <moneda>$payment_currency</moneda>
    <canal>W</canal>
    <omitir_notif_default>0</omitir_notif_default>
    <st_correo>0</st_correo>
    <mail_cliente>$email</mail_cliente>
  </url>
</P>
CHAIN;
    return $chain;
  }

  /**
   * Encrypt Chain text.
   */
  protected function encryptChain() {
    $originalString = $this->generateChain();
    $plugin = $this->plugin;
    $key = $plugin->getKey();
    try {
      return AESCrypto::encriptar($originalString, $key);
    }
    catch (Exception $e) {
      watchdog_exception('commerce_webpayplus', $e);
    }
  }

  /**
   * Generate the Link.
   */
  protected function generateLink() {
    $plugin = $this->plugin;
    $merchant = $plugin->getMerchant();
    $baseUrl = $plugin->getBaseUrl();
    $chain = $this->encryptChain();
    if ($merchant && $chain) {
      $xml = "<pgs><data0>$merchant</data0><data>$chain</data></pgs>";
      $encodedString = urlencode($xml);

      try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $baseUrl . '/gen');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
          'cache-control' => 'no-cache',
          'Content-Type' => 'application/x-www-form-urlencoded',
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "xml=$encodedString");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        $errorno = curl_errno($ch);
        if ($errorno) {
          \Drupal::logger('commerce_webpayplus')->notice(t('The url cannot be obtainerd, the result was error: %error and result: %result', ['%error' => $errorno, '%result' => $result]));
          return FALSE;
        }
        else {
          return $result;
        }
      }
      catch (Exception $e) {
        watchdog_exception('commerce_webpayplus', $e);
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Generate the Link.
   */
  protected function decryptLink() {
    $link = $this->generateLink();
    if ($link) {
      $plugin = $this->plugin;
      $key = $plugin->getKey();
      $linkEncrypted = AESCrypto::desencriptar($link, $key);
      $xml = simplexml_load_string($linkEncrypted);
      $json = json_encode($xml);
      $result = json_decode($json, TRUE);
      if (!empty($result['cd_response']) && $result['cd_response'] == 'success' && !empty($result['nb_url'])) {
        return $result['nb_url'];
      }
      else {
        \Drupal::logger('commerce_webpayplus')->notice(t('The url cannot be obtainerd, the result was %result', ['%result' => print_r($result, TRUE)]));
      }
      return FALSE;
    }
    return FALSE;
  }

}
